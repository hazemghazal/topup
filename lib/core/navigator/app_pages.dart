// Flutter imports:
import 'package:flutter/material.dart';
// Project imports:
import 'package:topup/core/navigator/app_navigator_observer.dart';
import 'package:topup/core/navigator/app_routes.dart';
import 'package:topup/core/navigator/scaffold_wrapper.dart';
import 'package:topup/core/util/sizer_custom/sizer.dart';
import 'package:topup/features/home/presentation/screens/add_ben/add_ben_screen.dart';
import 'package:topup/features/home/presentation/screens/home/home_screen.dart';
import 'package:topup/features/home/presentation/screens/topup/topup_screen.dart';

class AppNavigator extends RouteObserver<PageRoute<dynamic>> {
  static GlobalKey<NavigatorState> navigatorKey = GlobalKey();

  Route<dynamic> getRoute(RouteSettings settings) {
    Map<String, dynamic>? arguments = _getArguments(settings);
    switch (settings.name) {
      case Routes.rootRoute:
        return _buildRoute(
          settings,
          const HomeScreen(),
        );

      case Routes.topupRoute:
        return _buildRoute(
          settings,
          TopupScreen(
            phoneNumber: arguments?['phoneNumber'],
          ),
        );

      case Routes.addBenRoute:
        return _buildRoute(
          settings,
          const AddNewBeneficiary(),
        );

      default:
        return _buildRoute(
          settings,
          const HomeScreen(),
        );
    }
  }

  _buildRoute(
    RouteSettings routeSettings,
    Widget builder,
  ) {
    return MaterialPageRoute(
      builder: (context) => ScaffoldWrapper(
        child: builder,
      ),
      settings: routeSettings,
    );
  }

  static Future? push<T>(
    String route, {
    Object? arguments,
  }) {
    late NavigatorState stateByContext;

    stateByContext = state;

    return stateByContext.pushNamed(route, arguments: arguments);
  }

  static Future pushNamedAndRemoveUntil<T>(
    String route, {
    Object? arguments,
  }) {
    if (route == Routes.rootRoute) {
      AppNavigatorObserver.resetRoutes();
    }

    return state.pushNamedAndRemoveUntil(
      route,
      (route) => false,
      arguments: arguments,
    );
  }

  static Future? replaceWith<T>(
    String route, {
    Map<String, dynamic>? arguments,
  }) {
    return state.pushReplacementNamed(route, arguments: arguments);
  }

  static void popUntil<T>(String routeName) {
    state.popUntil((route) {
      if (route.isFirst) return true;

      return route.settings.name == routeName;
    });
  }

  static void pop({BuildContext? context}) {
    if (SizerUtil.isTablet) {
      if (context != null &&
          getRouteTablet(ModalRoute.of(context)?.settings.name ?? '')) {
        return;
      }
    }

    if (!canPop) return;

    state.pop();
  }

  _getArguments(RouteSettings settings) {
    return settings.arguments;
  }

  static bool get canPop => state.canPop();

  static String? currentRoute() => AppNavigatorObserver.currentRouteName;

  static BuildContext? get context => navigatorKey.currentContext;

  static BuildContext? get accountContext => navigatorKey.currentContext;

  static NavigatorState get state => navigatorKey.currentState!;

  static bool getRouteTablet(String route) => [].contains(route);
}
