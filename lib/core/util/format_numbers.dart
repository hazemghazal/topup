import 'package:number_display/number_display.dart';

String formatPhoneNumber(String phoneNumber) {
  if (phoneNumber.length == 10) {
    return phoneNumber.replaceFirstMapped(RegExp(r'^(\d{3})(\d{3})(\d{4})$'),
        (match) => '${match[1]} ${match[2]} ${match[3]}');
  } else {
    return phoneNumber;
  }
}

final display = createDisplay(length: 20);
