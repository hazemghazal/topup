// Dart imports:
import 'dart:developer' as developer;

// Flutter imports:
import 'package:flutter/foundation.dart';

class UtilLogger {
  static const String defaultName = "DEFAULT";

  static log(dynamic msg, {String? name}) {
    if (kDebugMode) {
      developer.log('$msg', name: defaultName);
    }
  }

  ///Singleton factory
  static final UtilLogger _instance = UtilLogger._internal();

  factory UtilLogger() {
    return _instance;
  }

  UtilLogger._internal();
}
