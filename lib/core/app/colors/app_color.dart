import 'package:flutter/material.dart';

Color colorBlack = const Color(0xFF121212);
Color colorPrimary = const Color(0xFF4E5C9B);

Color gradientFirst = const Color(0xFF4E5C9B);
Color gradientSecond = const Color(0xFF7387C9);

Color mC = Colors.grey.shade100;
Color mCL = Colors.white;
Color mCM = Colors.grey.shade200;
Color mCU = Colors.grey.shade300;
Color mCH = Colors.grey.shade400;
Color mGB = Colors.grey.shade500;
Color mGM = Colors.grey.shade700;
Color mGE = Colors.grey.shade800;
Color mGD = Colors.grey.shade900;
Color mCD = Colors.black.withOpacity(0.075);
Color mCC = Colors.green.withOpacity(0.65);
Color fCD = Colors.grey.shade700;
Color fCL = Colors.grey;

class AppColors {
  final Color primary;
  final Color primaryLight;
  final Color primaryDark;
  final Color background;
  final Color focusColor;
  final Color unFocusColor;
  final Color accent;
  final Color disabled;
  final Color error;
  final Color divider;
  final Color dividerBackgroundColor;
  final Color header;
  final Color button;
  final Color bodyLarge;
  final Color bodyMedium;
  final Color bodySmall;
  final Color displayLarge;
  final Color displayMedium;
  final Color displaySmall;

  const AppColors({
    required this.header,
    required this.primary,
    required this.primaryLight,
    required this.primaryDark,
    required this.background,
    required this.focusColor,
    required this.unFocusColor,
    required this.accent,
    required this.disabled,
    required this.error,
    required this.divider,
    required this.dividerBackgroundColor,
    required this.button,
    required this.bodyLarge,
    required this.bodyMedium,
    required this.bodySmall,
    required this.displayLarge,
    required this.displayMedium,
    required this.displaySmall,
  });

  factory AppColors.light() {
    return AppColors(
      header: colorPrimary,
      primary: colorPrimary,
      primaryLight: mCL,
      primaryDark: colorBlack,
      background: Colors.white,
      focusColor: Colors.green,
      unFocusColor: Colors.grey,
      accent: const Color(0xFF17c063),
      disabled: Colors.black12,
      error: const Color(0xFFFF7466),
      divider: Colors.black26,
      dividerBackgroundColor: Colors.black54,
      button: const Color(0xFF657786),
      bodyLarge: colorBlack,
      bodyMedium: colorBlack,
      bodySmall: colorBlack,
      displayLarge: mGD,
      displayMedium: mGB,
      displaySmall: mGB,
    );
  }
}
