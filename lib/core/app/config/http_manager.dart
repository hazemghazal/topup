// ignore_for_file: avoid_function_literals_in_foreach_calls

import 'dart:developer';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:topup/core/app/constant/endpoints.dart';
import 'package:topup/core/error/failures.dart';
import 'package:topup/core/util/logger.dart';

Map<String, dynamic> dioErrorHandle(
    {DioException? error, String? stringError}) {
  if (error != null) {
    log(
      error.toString(),
      name: "ERROR",
    );
  } else {
    log(
      stringError!,
      name: "ERROR",
    );
  }
  if (error != null) {
    switch (error.type) {
      case DioExceptionType.badResponse:
        {
          UtilLogger.log(error.response.toString());

          return Failure.serverError;
        }
      default:
        {
          return Failure.serverError;
        }
    }
  } else {
    return Failure.serverError;
  }
}

@Singleton()
class HTTPManager {
  BaseOptions baseOptions = BaseOptions(
    followRedirects: false,
    baseUrl: Endpoints.baseURL,
    validateStatus: (status) {
      return status! < 500;
    },
    contentType: Headers.formUrlEncodedContentType,
    responseType: ResponseType.json,
  );

  BaseOptions exportOption(BaseOptions options) {
    Map<String, dynamic> header = {
      "Accept": "application/json",
    };
    options.headers.addAll(header);

    /// Implement Token
    // String? token = Preferences.getString(key: Preferences.keyToken);

    // if (token.isNotEmpty) {
    //   options.headers["Authorization"] = token;
    // }
    return options;
  }

  Future<bool> download(String url, String savePath) async {
    Dio dio = Dio();
    try {
      Response response = await dio.download(
        url,
        savePath,
      );
      if (response.statusCode! >= 200 && response.statusCode! <= 300) {
        return true;
      }
      return false;
    } catch (e) {
      log("download err $e");
      return false;
    }
  }

  Future<dynamic> put({
    required String url,
    required Map<String, dynamic> data,
  }) async {
    Dio dio = Dio(exportOption(baseOptions));
    if (await InternetConnectionChecker().hasConnection) {
      log(
        url,
        name: "PUT URL",
      );
      log(
        data.toString(),
        name: "DATA",
      );
      try {
        final response = await dio.put(
          url,
          data: data,
        );
        log(
          response.toString(),
          name: "PUT Response: $url",
        );
        if (response.data.toString().contains("Unauthenticated.")) {}
        return response.data;
      } on DioException catch (error) {
        return dioErrorHandle(error: error);
      }
    } else {
      return dioErrorHandle(stringError: "network error");
    }
  }

  bool _isUnAuthHappened = false;

  Future<dynamic> post(
      {required String url,
      Map<String, dynamic>? data,
      FormData? formData,
      Map<String, List<File>?>? files}) async {
    Dio dio = Dio(exportOption(baseOptions));
    if (await InternetConnectionChecker().hasConnection) {
      log(
        "${dio.options.baseUrl} $url",
        name: "POST URL",
      );

      if (formData != null) {
        formData.fields.forEach((element) {
          log("${element.key} - ${element.value}");
        });
      }
      if (data != null) {
        log(
          data.toString(),
          name: "POST DATA",
        );
      }

      try {
        if (formData != null && files != null && files.isNotEmpty) {
          files.forEach((key, value) async {
            List<File>? files = value;

            if (files != null && files.isNotEmpty) {
              files.forEach((file) async {
                String fileName = file.path.split('/').last.trim();

                if (fileName.contains(' ')) {
                  fileName = fileName.replaceAll(' ', '');
                }
                formData.files.add(MapEntry(
                    key,
                    await MultipartFile.fromFile(file.path,
                        filename: fileName)));

                log("$fileName added to form data");
              });
            }
          });
        }
        final response = await dio.post(
          url,
          data: data ?? formData,
        );

        log(
          response.toString(),
          name: "POST Response: $url",
        );
        log(
          response.statusCode.toString(),
          name: "POST Status Code: $url",
        );
        if (response.statusCode == 401) {
          log("Unauthorized access");
          if (_isUnAuthHappened) return;

          ///Implement Logout
          // OnLogout();
          log("login out now...");
          _isUnAuthHappened = true;
        }
        await _saveToken(response);
        return response.data;
      } on DioException catch (error) {
        return dioErrorHandle(error: error);
      }
    } else {
      return dioErrorHandle(stringError: "network error");
    }
  }

  Future<dynamic> get({
    required String url,
    Map<String, dynamic>? params,
  }) async {
    Dio dio = Dio(exportOption(baseOptions));
    if (await InternetConnectionChecker().hasConnection) {
      log(
        "${dio.options.baseUrl} $url",
        name: "GET URL",
      );

      if (params != null) {
        log(
          params.toString(),
          name: "PARAMS",
        );
      }
      try {
        final response = await dio.get(url, queryParameters: params);
        log(
          response.toString(),
          name: "GET Response: $url",
        );

        if (response.data.toString().contains("Unauthenticated.")) {}
        return response.data;
      } on DioException catch (error) {
        return dioErrorHandle(error: error);
      }
    } else {
      return dioErrorHandle(stringError: "network error");
    }
  }

  Future<dynamic> delete({
    required String url,
    Map<String, dynamic>? data,
  }) async {
    Dio dio = Dio(exportOption(baseOptions));
    if (await InternetConnectionChecker().hasConnection) {
      log(
        url,
        name: "Delete URL",
      );
      if (data != null) {
        log(
          data.toString(),
          name: "PARAMS",
        );
      }
      try {
        final response = await dio.delete(url, data: data);
        log(
          response.toString(),
          name: "Delete Response: $url",
        );

        if (response.data.toString().contains("Unauthenticated.")) {}
        return response.data;
      } on DioException catch (error) {
        return dioErrorHandle(error: error);
      }
    } else {
      return dioErrorHandle(stringError: "network error");
    }
  }

  _saveToken(Response response) async {
    if (response.headers.map.isEmpty) return;

    if (response.statusCode! >= 200 && response.statusCode! <= 300) {
      if (response.headers['authorization'] != null &&
          response.headers['authorization']!.isNotEmpty) {
        log("token saved");
        // Preferences.save(
        //     key: Preferences.keyToken,
        //     data: 'Bearer ${response.headers['authorization']!.first}');
      }
    }
  }
}
