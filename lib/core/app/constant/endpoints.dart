class Endpoints {
  // Remote Data
  static const String baseURL = 'https://test.ae/';
  // Fetch User
  static const String user = 'me';
  // Fetch Phone Numbers
  static const String phoneNumbers = 'user_phone_numbers';
  // Post Recharge
  static const String recharge = 'recharge';
  // Post Add Beneficiary
  static const String addBeneficiary = 'add_ben';
}
