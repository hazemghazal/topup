// Dart imports:
import 'dart:io';

// Flutter imports:
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
// Project imports:
import 'package:topup/core/app/colors/app_color.dart';
import 'package:topup/core/app/constant/constants.dart';
import 'package:topup/core/util/sizer_custom/sizer.dart';

class AppTheme {
  AppTheme({
    required this.data,
  });

  factory AppTheme.light({bool isChild = false}) {
    final double fontSize = 20.sp;
    final appColors = AppColors.light();
    final themeData = ThemeData(
      useMaterial3: true,
      pageTransitionsTheme: isChild
          ? const PageTransitionsTheme(builders: {
              TargetPlatform.iOS: ZoomPageTransitionsBuilder(),
              TargetPlatform.android: ZoomPageTransitionsBuilder(),
            })
          : const PageTransitionsTheme(builders: {
              TargetPlatform.iOS: CupertinoPageTransitionsBuilder(),
              TargetPlatform.android: CupertinoPageTransitionsBuilder(),
            }),
      brightness: Brightness.light,
      primaryColor: appColors.primary,
      primaryColorLight: appColors.primaryLight,
      primaryColorDark: appColors.primaryDark,
      focusColor: appColors.focusColor,
      disabledColor: appColors.unFocusColor,
      scaffoldBackgroundColor: appColors.background,
      snackBarTheme: SnackBarThemeData(
        backgroundColor: appColors.error,
        behavior: SnackBarBehavior.floating,
      ),
      bottomNavigationBarTheme: BottomNavigationBarThemeData(
        backgroundColor: appColors.background,
        selectedItemColor: colorPrimary,
      ),
      cardTheme: CardTheme(
          color: appColors.background, surfaceTintColor: appColors.background),
      appBarTheme: AppBarTheme(
        scrolledUnderElevation: 0,
        surfaceTintColor: appColors.background,
        backgroundColor: appColors.background,
        systemOverlayStyle: SystemUiOverlayStyle(
          statusBarColor: Colors.transparent,
          statusBarBrightness: Brightness.light ==
                  (Platform.isAndroid ? Brightness.dark : Brightness.light)
              ? Brightness.light
              : Brightness.dark,
          statusBarIconBrightness: Brightness.light ==
                  (Platform.isAndroid ? Brightness.dark : Brightness.light)
              ? Brightness.light
              : Brightness.dark,
        ),
        iconTheme: IconThemeData(
          color: appColors.bodyLarge,
        ),
      ),
      textTheme: TextTheme(
        headlineLarge: TextStyle(
            color: appColors.header,
            fontSize: fontSize,
            fontFamily: fontFamily,
            fontWeight: FontWeight.bold),
        headlineMedium: TextStyle(
            color: appColors.header,
            fontFamily: fontFamily,
            fontSize: fontSize - 2,
            fontWeight: FontWeight.bold),
        headlineSmall: TextStyle(
            color: appColors.header,
            fontFamily: fontFamily,
            fontSize: fontSize - 4,
            fontWeight: FontWeight.bold),
        bodyLarge: TextStyle(
            color: appColors.bodyLarge,
            fontSize: fontSize - 1,
            fontFamily: fontFamily,
            fontWeight: FontWeight.w600),
        bodyMedium: TextStyle(
            color: appColors.bodyMedium,
            fontSize: fontSize - 3,
            fontFamily: fontFamily,
            fontWeight: FontWeight.w600),
        bodySmall: TextStyle(
            color: appColors.bodySmall,
            fontSize: fontSize - 5,
            fontFamily: fontFamily,
            fontWeight: FontWeight.w600),
        displayLarge: TextStyle(
            color: appColors.displayLarge,
            fontSize: fontSize - 1,
            fontFamily: fontFamily,
            fontWeight: FontWeight.w500),
        displayMedium: TextStyle(
            color: appColors.displayMedium,
            fontSize: fontSize - 3,
            fontFamily: fontFamily,
            fontWeight: FontWeight.w500),
        displaySmall: TextStyle(
            color: appColors.displaySmall,
            fontSize: fontSize - 5,
            fontFamily: fontFamily,
            fontWeight: FontWeight.w500),
      ),
      inputDecorationTheme: InputDecorationTheme(
        fillColor: mCU,
      ),
      dividerColor: appColors.divider,
    );
    return AppTheme(
      data: themeData,
    );
  }

  final ThemeData data;
}
