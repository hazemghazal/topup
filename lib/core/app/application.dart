// Flutter imports:
import 'package:flutter/material.dart';
import 'package:topup/core/injection/injection_container.dart';

class Application {
  /// [Production - Dev]
  static Future<void> initialApplication() async {
    try {
      // Init dependency injection
      configureDependencies();
    } catch (error) {
      debugPrint(error.toString());
    }
  }

  ///Singleton factory
  static final Application _instance = Application._internal();

  factory Application() {
    return _instance;
  }

  Application._internal();
}
