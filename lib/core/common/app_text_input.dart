import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class AppTextInput extends StatelessWidget {
  final AppTextFieldParameters appTextFieldParameters;

  const AppTextInput({
    super.key,
    required this.appTextFieldParameters,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: const EdgeInsets.all(2.5),
      decoration: BoxDecoration(
        color: appTextFieldParameters.enabled ||
                appTextFieldParameters.ignoreEnableColor
            ? Theme.of(context).inputDecorationTheme.fillColor
            : Theme.of(context).canvasColor.withOpacity(0.4),
        borderRadius: BorderRadius.circular(appTextFieldParameters.radius),
      ),
      child: Center(
        child: TextFormField(
          focusNode: appTextFieldParameters.focusNode,
          expands: appTextFieldParameters.expands,
          inputFormatters: appTextFieldParameters.inputFormatters,
          enabled: appTextFieldParameters.enabled,
          readOnly: appTextFieldParameters.readOnly,
          controller: appTextFieldParameters.controller,
          style: Theme.of(context).textTheme.bodyMedium!.copyWith(
              decoration: TextDecoration.none,
              fontSize: appTextFieldParameters.textFontSize),
          onChanged: (value) {
            if (appTextFieldParameters.onChanged != null) {
              appTextFieldParameters.onChanged!(value);
            }
          },
          obscureText: appTextFieldParameters.obscureText,
          keyboardType: appTextFieldParameters.textInputType,
          textInputAction: appTextFieldParameters.textInputAction,
          maxLines: appTextFieldParameters.maxLines,
          onFieldSubmitted: (value) {
            if (appTextFieldParameters.onSubmit != null) {
              appTextFieldParameters.onSubmit!(value);
            }
          },
          minLines: appTextFieldParameters.minLines,
          cursorColor: Theme.of(context).colorScheme.secondary,
          validator: (value) {
            if (!appTextFieldParameters.isRequired!) return null;

            if (value == null || value.isEmpty) {
              return 'This field is required';
            }

            if (appTextFieldParameters.regex != null &&
                !appTextFieldParameters.regex!.hasMatch(value)) {
              return 'This is not a valid value';
            }

            return null;
          },
          textAlignVertical: appTextFieldParameters.suffix != null ||
                  appTextFieldParameters.prefix != null
              ? TextAlignVertical.center
              : null,
          decoration: InputDecoration(
            alignLabelWithHint: true,
            floatingLabelBehavior: FloatingLabelBehavior.always,
            contentPadding: EdgeInsets.fromLTRB(20.0, 10.0,
                appTextFieldParameters.suffix != null ? 20.0 : 10, 10.0),
            hintText: appTextFieldParameters.hintText,
            suffixIcon: appTextFieldParameters.suffix,
            prefixIcon: appTextFieldParameters.prefix,
            hintStyle: Theme.of(context).textTheme.displaySmall,
            labelStyle: Theme.of(context).textTheme.bodyMedium,
            errorStyle: Theme.of(context).textTheme.bodyMedium,
            focusedErrorBorder: InputBorder.none,
            focusedBorder: InputBorder.none,
            enabledBorder: InputBorder.none,
            errorBorder: InputBorder.none,
            disabledBorder: InputBorder.none,
          ),
        ),
      ),
    );
  }
}

class AppTextFieldParameters {
  final TextEditingController? controller;
  final bool obscureText;
  final String hintText;
  final TextInputType textInputType;
  final List<TextInputFormatter>? inputFormatters;
  final Widget? suffix;
  final Widget? prefix;
  final bool? isRequired;
  final bool enabled;
  final bool expands;
  final bool readOnly;
  final double radius;
  final RegExp? regex;
  final TextInputAction? textInputAction;
  final int? maxLines;
  final int? minLines;
  final Function? onChanged;
  final Function? onSubmit;
  final bool ignoreEnableColor;
  final FocusNode? focusNode;
  final double textFontSize;

  AppTextFieldParameters({
    this.controller,
    this.obscureText = false,
    this.hintText = "",
    this.textInputType = TextInputType.text,
    this.inputFormatters,
    this.suffix,
    this.prefix,
    this.readOnly = false,
    this.isRequired = false,
    this.expands = false,
    this.enabled = true,
    this.ignoreEnableColor = false,
    this.regex,
    this.textInputAction,
    this.maxLines = 1,
    this.minLines,
    this.onChanged,
    this.onSubmit,
    this.focusNode,
    this.radius = 15,
    this.textFontSize = 16,
  });
}
