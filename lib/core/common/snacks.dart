import 'package:flutter/material.dart';
import 'package:topup/core/navigator/app_pages.dart';

enum SnackTypes { fail, warning, success }

class SnackHelper {
  static showAppDialog({required String message, required SnackTypes type}) {
    final snackBar = SnackBar(
      elevation: 0,
      margin: const EdgeInsets.only(bottom: 70),
      backgroundColor: _getBackgroundColor(type),
      content: Row(
        children: [
          Flexible(
            child: Text(
              message,
              style: Theme.of(AppNavigator.navigatorKey.currentContext!)
                  .textTheme
                  .titleSmall,
            ),
          ),
        ],
      ),
    );

    ScaffoldMessenger.of(AppNavigator.navigatorKey.currentContext!)
      ..hideCurrentSnackBar()
      ..showSnackBar(snackBar);
  }

  static Color _getBackgroundColor(SnackTypes type) {
    switch (type) {
      case SnackTypes.fail:
        return Colors.red[300]!;
      case SnackTypes.warning:
        return Colors.yellow[300]!;
      case SnackTypes.success:
        return Colors.green[300]!;
      default:
        return Colors.grey;
    }
  }
}
