import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:topup/core/app/colors/app_color.dart';
import 'package:topup/core/util/sizer_custom/sizer.dart';

import 'button_parameters.dart';

class MainButtonElevated extends StatelessWidget {
  final MainButtonParameters mainButtonParameters;

  const MainButtonElevated({Key? key, required this.mainButtonParameters})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      borderRadius: BorderRadius.circular(mainButtonParameters.radius),
      onTap: mainButtonParameters.onClick == null ||
              mainButtonParameters.isLoading ||
              mainButtonParameters.isDisable
          ? null
          : () {
              mainButtonParameters.onClick!();
            },
      child: Container(
        height: mainButtonParameters.height,
        width: mainButtonParameters.width,
        decoration:
            mainButtonParameters.isLoading || mainButtonParameters.isDisable
                ? BoxDecoration(
                    color: mGB,
                    borderRadius:
                        BorderRadius.circular(mainButtonParameters.radius),
                  )
                : BoxDecoration(
                    boxShadow: mainButtonParameters.noShadow
                        ? null
                        : <BoxShadow>[
                            BoxShadow(
                              color: mainButtonParameters.shadowColor ??
                                  mGB.withOpacity(0.5),
                              blurRadius: 4,
                              offset: const Offset(4, 8),
                            ),
                          ],
                    gradient: mainButtonParameters.isLoading ||
                            mainButtonParameters.isDisable
                        ? null
                        : LinearGradient(
                            colors: [
                              gradientFirst,
                              gradientSecond,
                            ],
                          ),
                    color: mainButtonParameters.isLoading ||
                            mainButtonParameters.isDisable
                        ? null
                        : mainButtonParameters.bgColor ?? colorPrimary,
                    borderRadius:
                        BorderRadius.circular(mainButtonParameters.radius),
                  ),
        child: Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              if (mainButtonParameters.prefix != null &&
                  !mainButtonParameters.isLoading)
                Padding(
                  padding: EdgeInsets.only(right: 5.sp),
                  child: mainButtonParameters.prefix!,
                ),
              if (mainButtonParameters.isLoading) ...{
                Flexible(
                  child: Lottie.asset(
                    "assets/Animations/loading.json",
                    fit: BoxFit.fill,
                  ),
                )
              } else ...{
                Padding(
                  padding: EdgeInsets.only(top: 5.sp),
                  child: Text(
                    mainButtonParameters.label,
                    style: mainButtonParameters.labelStyle ??
                        Theme.of(context)
                            .textTheme
                            .displaySmall!
                            .copyWith(color: Colors.white),
                  ),
                )
              },
              if (mainButtonParameters.suffix != null &&
                  !mainButtonParameters.isLoading)
                Padding(
                  padding: EdgeInsets.only(left: 5.sp),
                  child: mainButtonParameters.suffix!,
                ),
            ],
          ),
        ),
      ),
    );
  }
}
