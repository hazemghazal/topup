import 'package:flutter/cupertino.dart';

class MainButtonParameters {
  final String label;
  final Function? onClick;
  final Widget? prefix;
  final Widget? suffix;
  final bool cancelBtn;
  final bool greenBtn;
  final bool noShadow;
  final bool isLoading;
  final bool isDisable;
  final double width;
  final TextStyle? labelStyle;
  final double height;
  final double radius;
  final Color? bgColor;
  final Color? shadowColor;

  MainButtonParameters({
    required this.label,
    this.onClick,
    this.noShadow = false,
    this.cancelBtn = false,
    this.greenBtn = false,
    this.prefix,
    this.labelStyle,
    this.suffix,
    this.width = double.infinity,
    this.height = 55,
    this.radius = 25,
    this.bgColor,
    this.shadowColor,
    this.isLoading = false,
    this.isDisable = false,
  });
}
