import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:topup/core/util/sizer_custom/sizer.dart';

import 'button_parameters.dart';

class MainButtonOutlined extends StatelessWidget {
  final MainButtonParameters mainButtonParameters;

  const MainButtonOutlined({Key? key, required this.mainButtonParameters})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: mainButtonParameters.height,
      width: mainButtonParameters.width,
      child: OutlinedButton(
        style: OutlinedButton.styleFrom(
          foregroundColor: mainButtonParameters.bgColor ??
              Theme.of(context).colorScheme.secondary,
          side: BorderSide(
              width: 1.0,
              color: mainButtonParameters.bgColor ??
                  Theme.of(context).colorScheme.secondary),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(mainButtonParameters.radius),
          ),
        ),
        onPressed: mainButtonParameters.onClick == null
            ? null
            : () {
                mainButtonParameters.onClick!();
              },
        child: Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              if (mainButtonParameters.prefix != null)
                Padding(
                  padding: EdgeInsets.only(right: 5.sp),
                  child: mainButtonParameters.prefix!,
                ),
              if (mainButtonParameters.isLoading) ...{
                Lottie.asset(
                  "assets/Animations/loading.json",
                  fit: BoxFit.fill,
                )
              } else ...{
                Text(
                  mainButtonParameters.label,
                  style: mainButtonParameters.labelStyle,
                ),
              },
              if (mainButtonParameters.suffix != null)
                Padding(
                  padding: EdgeInsets.only(left: 5.sp),
                  child: mainButtonParameters.suffix!,
                ),
            ],
          ),
        ),
      ),
    );
  }
}
