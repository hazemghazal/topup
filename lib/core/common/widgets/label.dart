import 'package:flutter/material.dart';
import 'package:topup/core/util/sizer_custom/sizer.dart';

class LabelWidget extends StatelessWidget {
  final String label;
  const LabelWidget({super.key, required this.label});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        SizedBox(
          width: 20.sp,
        ),
        Text(
          label,
          style: Theme.of(context).textTheme.bodyMedium,
        ),
      ],
    );
  }
}
