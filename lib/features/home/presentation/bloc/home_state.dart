import 'package:built_value/built_value.dart';
import 'package:topup/features/home/data/model/phone_number_model.dart';
import 'package:topup/features/home/data/model/user_model.dart';

part 'home_state.g.dart';

abstract class HomeState implements Built<HomeState, HomeStateBuilder> {
  /// Loading Handel
  bool get isLoadingUser;
  bool get isLoadingPhoneNumber;
  bool get isLoadingAddBen;
  bool get isLoadingRecharge;

  /// DATA
  UserModel get user;
  List<PhoneNumberModel> get phoneNumbersList;

  /// UI
  double get selectedRechargeAmount;

  HomeState._();

  factory HomeState([Function(HomeStateBuilder b) updates]) = _$HomeState;

  factory HomeState.initial() {
    return HomeState((b) => b
      ..isLoadingUser = false
      ..isLoadingPhoneNumber = false
      ..isLoadingAddBen = false
      ..isLoadingRecharge = false
      ..selectedRechargeAmount = 0
      ..user = UserModel()
      ..phoneNumbersList = []);
  }
}
