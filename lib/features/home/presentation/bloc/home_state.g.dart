// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'home_state.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$HomeState extends HomeState {
  @override
  final bool isLoadingUser;
  @override
  final bool isLoadingPhoneNumber;
  @override
  final bool isLoadingAddBen;
  @override
  final bool isLoadingRecharge;
  @override
  final UserModel user;
  @override
  final List<PhoneNumberModel> phoneNumbersList;
  @override
  final double selectedRechargeAmount;

  factory _$HomeState([void Function(HomeStateBuilder)? updates]) =>
      (new HomeStateBuilder()..update(updates))._build();

  _$HomeState._(
      {required this.isLoadingUser,
      required this.isLoadingPhoneNumber,
      required this.isLoadingAddBen,
      required this.isLoadingRecharge,
      required this.user,
      required this.phoneNumbersList,
      required this.selectedRechargeAmount})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        isLoadingUser, r'HomeState', 'isLoadingUser');
    BuiltValueNullFieldError.checkNotNull(
        isLoadingPhoneNumber, r'HomeState', 'isLoadingPhoneNumber');
    BuiltValueNullFieldError.checkNotNull(
        isLoadingAddBen, r'HomeState', 'isLoadingAddBen');
    BuiltValueNullFieldError.checkNotNull(
        isLoadingRecharge, r'HomeState', 'isLoadingRecharge');
    BuiltValueNullFieldError.checkNotNull(user, r'HomeState', 'user');
    BuiltValueNullFieldError.checkNotNull(
        phoneNumbersList, r'HomeState', 'phoneNumbersList');
    BuiltValueNullFieldError.checkNotNull(
        selectedRechargeAmount, r'HomeState', 'selectedRechargeAmount');
  }

  @override
  HomeState rebuild(void Function(HomeStateBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  HomeStateBuilder toBuilder() => new HomeStateBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is HomeState &&
        isLoadingUser == other.isLoadingUser &&
        isLoadingPhoneNumber == other.isLoadingPhoneNumber &&
        isLoadingAddBen == other.isLoadingAddBen &&
        isLoadingRecharge == other.isLoadingRecharge &&
        user == other.user &&
        phoneNumbersList == other.phoneNumbersList &&
        selectedRechargeAmount == other.selectedRechargeAmount;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, isLoadingUser.hashCode);
    _$hash = $jc(_$hash, isLoadingPhoneNumber.hashCode);
    _$hash = $jc(_$hash, isLoadingAddBen.hashCode);
    _$hash = $jc(_$hash, isLoadingRecharge.hashCode);
    _$hash = $jc(_$hash, user.hashCode);
    _$hash = $jc(_$hash, phoneNumbersList.hashCode);
    _$hash = $jc(_$hash, selectedRechargeAmount.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'HomeState')
          ..add('isLoadingUser', isLoadingUser)
          ..add('isLoadingPhoneNumber', isLoadingPhoneNumber)
          ..add('isLoadingAddBen', isLoadingAddBen)
          ..add('isLoadingRecharge', isLoadingRecharge)
          ..add('user', user)
          ..add('phoneNumbersList', phoneNumbersList)
          ..add('selectedRechargeAmount', selectedRechargeAmount))
        .toString();
  }
}

class HomeStateBuilder implements Builder<HomeState, HomeStateBuilder> {
  _$HomeState? _$v;

  bool? _isLoadingUser;
  bool? get isLoadingUser => _$this._isLoadingUser;
  set isLoadingUser(bool? isLoadingUser) =>
      _$this._isLoadingUser = isLoadingUser;

  bool? _isLoadingPhoneNumber;
  bool? get isLoadingPhoneNumber => _$this._isLoadingPhoneNumber;
  set isLoadingPhoneNumber(bool? isLoadingPhoneNumber) =>
      _$this._isLoadingPhoneNumber = isLoadingPhoneNumber;

  bool? _isLoadingAddBen;
  bool? get isLoadingAddBen => _$this._isLoadingAddBen;
  set isLoadingAddBen(bool? isLoadingAddBen) =>
      _$this._isLoadingAddBen = isLoadingAddBen;

  bool? _isLoadingRecharge;
  bool? get isLoadingRecharge => _$this._isLoadingRecharge;
  set isLoadingRecharge(bool? isLoadingRecharge) =>
      _$this._isLoadingRecharge = isLoadingRecharge;

  UserModel? _user;
  UserModel? get user => _$this._user;
  set user(UserModel? user) => _$this._user = user;

  List<PhoneNumberModel>? _phoneNumbersList;
  List<PhoneNumberModel>? get phoneNumbersList => _$this._phoneNumbersList;
  set phoneNumbersList(List<PhoneNumberModel>? phoneNumbersList) =>
      _$this._phoneNumbersList = phoneNumbersList;

  double? _selectedRechargeAmount;
  double? get selectedRechargeAmount => _$this._selectedRechargeAmount;
  set selectedRechargeAmount(double? selectedRechargeAmount) =>
      _$this._selectedRechargeAmount = selectedRechargeAmount;

  HomeStateBuilder();

  HomeStateBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _isLoadingUser = $v.isLoadingUser;
      _isLoadingPhoneNumber = $v.isLoadingPhoneNumber;
      _isLoadingAddBen = $v.isLoadingAddBen;
      _isLoadingRecharge = $v.isLoadingRecharge;
      _user = $v.user;
      _phoneNumbersList = $v.phoneNumbersList;
      _selectedRechargeAmount = $v.selectedRechargeAmount;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(HomeState other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$HomeState;
  }

  @override
  void update(void Function(HomeStateBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  HomeState build() => _build();

  _$HomeState _build() {
    final _$result = _$v ??
        new _$HomeState._(
            isLoadingUser: BuiltValueNullFieldError.checkNotNull(
                isLoadingUser, r'HomeState', 'isLoadingUser'),
            isLoadingPhoneNumber: BuiltValueNullFieldError.checkNotNull(
                isLoadingPhoneNumber, r'HomeState', 'isLoadingPhoneNumber'),
            isLoadingAddBen: BuiltValueNullFieldError.checkNotNull(
                isLoadingAddBen, r'HomeState', 'isLoadingAddBen'),
            isLoadingRecharge: BuiltValueNullFieldError.checkNotNull(
                isLoadingRecharge, r'HomeState', 'isLoadingRecharge'),
            user: BuiltValueNullFieldError.checkNotNull(
                user, r'HomeState', 'user'),
            phoneNumbersList: BuiltValueNullFieldError.checkNotNull(
                phoneNumbersList, r'HomeState', 'phoneNumbersList'),
            selectedRechargeAmount: BuiltValueNullFieldError.checkNotNull(
                selectedRechargeAmount,
                r'HomeState',
                'selectedRechargeAmount'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
