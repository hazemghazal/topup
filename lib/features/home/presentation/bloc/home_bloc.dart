import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:topup/core/common/snacks.dart';
import 'package:topup/core/injection/injection_container.dart';
import 'package:topup/core/navigator/app_pages.dart';
import 'package:topup/core/navigator/app_routes.dart';
import 'package:topup/features/home/data/model/phone_number_model.dart';
import 'package:topup/features/home/data/model/user_model.dart';
import 'package:topup/features/home/domain/repositories/topup_repository.dart';

import 'home_event.dart';
import 'home_state.dart';

@LazySingleton()
class HomeBloc extends Bloc<HomeEvent, HomeState> {
  final TopupRepository topupRepository = getIt<TopupRepository>();
  HomeBloc() : super(HomeState.initial()) {
    on<OnSelectAmount>((event, emit) async {
      emit(state.rebuild((b) => b..selectedRechargeAmount = event.amount));
    });

    on<OnFetchUser>((event, emit) async {
      emit(state.rebuild((b) => b..isLoadingUser = true));
      final result = await topupRepository.getUser();

      /// Future Delay
      await Future.delayed(const Duration(seconds: 1));
      result.fold(
        (left) {
          emit(state.rebuild((b) => b..isLoadingUser = false));
          SnackHelper.showAppDialog(message: left, type: SnackTypes.fail);
        },
        (right) {
          emit(state.rebuild((b) => b
            ..isLoadingUser = false
            ..user = right));
        },
      );
    });

    on<OnFetchNumbers>((event, emit) async {
      emit(state.rebuild((b) => b..isLoadingPhoneNumber = true));
      final result = await topupRepository.getPhoneNumbers();

      /// Future Delay
      await Future.delayed(const Duration(seconds: 1));
      result.fold(
        (left) {
          emit(state.rebuild((b) => b..isLoadingPhoneNumber = false));
          SnackHelper.showAppDialog(message: left, type: SnackTypes.fail);
        },
        (right) {
          emit(state.rebuild((b) => b
            ..isLoadingPhoneNumber = false
            ..phoneNumbersList = right));
        },
      );
    });

    on<OnAddBeneficiary>((event, emit) async {
      emit(state.rebuild((b) => b..isLoadingAddBen = true));
      final result = await topupRepository.addBen(param: event.param);

      /// Future Delay
      await Future.delayed(const Duration(seconds: 1));

      result.fold(
        (left) {
          emit(state.rebuild((b) => b..isLoadingAddBen = false));
          SnackHelper.showAppDialog(message: left, type: SnackTypes.fail);
        },
        (right) {
          List<PhoneNumberModel> data = state.phoneNumbersList;

          data.insert(
              0,
              PhoneNumberModel(
                id: right.id,
                phone_number: right.phone_number,
                nick_name: right.nick_name,
              ));
          emit(state.rebuild((b) => b
            ..isLoadingAddBen = false
            ..phoneNumbersList = data));
          AppNavigator.popUntil(Routes.rootRoute);
        },
      );
    });

    on<OnRecharge>((event, emit) async {
      if ((state.selectedRechargeAmount + 1) > state.user.balance) {
        SnackHelper.showAppDialog(
            message: "The recharge amount is more than the available credits",
            type: SnackTypes.warning);
        return;
      }
      double totalUserCurrentMonthTransactions =
          state.user.total_current_month_transactions +
              event.param.recharge_amount;
      if (totalUserCurrentMonthTransactions > 3000) {
        SnackHelper.showAppDialog(
            message:
                "You've reached your limit (a maximum of AED 3,000 per calendar month).",
            type: SnackTypes.warning);
        return;
      }

      if (state.user.verified! &&
          event.phoneNumberModel.total_transactions_per_month > 500) {
        SnackHelper.showAppDialog(
            message:
                "You've reached the limit for this beneficiary (a maximum of AED 500 per calendar month).",
            type: SnackTypes.warning);
        return;
      }

      if (!state.user.verified! &&
          event.phoneNumberModel.total_transactions_per_month > 500) {
        SnackHelper.showAppDialog(
            message:
                "You've reached the limit for this beneficiary (a maximum of AED 1000 per calendar month).",
            type: SnackTypes.warning);
        return;
      }

      emit(state.rebuild((b) => b..isLoadingRecharge = true));

      UserModel newUserData = state.user.copyWith(
          balance: state.user.balance - (state.selectedRechargeAmount + 1));

      final result = await topupRepository.recharge(param: event.param);

      /// Future Delay
      await Future.delayed(const Duration(seconds: 1));
      result.fold(
        (left) {
          emit(state.rebuild((b) => b..isLoadingRecharge = false));
          SnackHelper.showAppDialog(message: left, type: SnackTypes.fail);
        },
        (right) {
          emit(state.rebuild((b) => b
            ..isLoadingRecharge = false
            ..user = newUserData));
          SnackHelper.showAppDialog(message: right, type: SnackTypes.success);
          AppNavigator.popUntil(Routes.rootRoute);
        },
      );
    });
  }
}
