import 'package:equatable/equatable.dart';
import 'package:topup/features/home/data/model/phone_number_model.dart';
import 'package:topup/features/home/domain/entities/add_ben.dart';
import 'package:topup/features/home/domain/entities/recharge.dart';

abstract class HomeEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class OnSelectAmount extends HomeEvent {
  final double amount;

  OnSelectAmount({required this.amount});
}

class OnFetchUser extends HomeEvent {}

class OnFetchNumbers extends HomeEvent {}

class OnAddBeneficiary extends HomeEvent {
  final AddBenParam param;

  OnAddBeneficiary({required this.param});
}

class OnRecharge extends HomeEvent {
  final RechargeParam param;
  final PhoneNumberModel phoneNumberModel;

  OnRecharge({
    required this.param,
    required this.phoneNumberModel,
  });
}
