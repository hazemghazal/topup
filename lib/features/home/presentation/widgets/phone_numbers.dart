import 'package:flutter/material.dart';
import 'package:topup/core/common/widgets/buttons/button_parameters.dart';
import 'package:topup/core/common/widgets/buttons/main_btn.dart';
import 'package:topup/core/navigator/app_pages.dart';
import 'package:topup/core/navigator/app_routes.dart';
import 'package:topup/core/util/format_numbers.dart';
import 'package:topup/core/util/sizer_custom/sizer.dart';
import 'package:topup/features/home/data/model/phone_number_model.dart';

class ShowPhoneNumber extends StatelessWidget {
  final PhoneNumberModel phoneNumber;
  const ShowPhoneNumber({super.key, required this.phoneNumber});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 140.sp,
      child: Card(
        elevation: 5,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15),
        ),
        child: Column(
          children: [
            SizedBox(
              height: 10.sp,
            ),
            SizedBox(
              height: 22.sp,
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: FittedBox(
                  fit: BoxFit.fitWidth,
                  child: Text(
                    phoneNumber.nick_name ?? "",
                    style: Theme.of(context).textTheme.headlineSmall,
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 5.sp,
            ),
            Text(
              formatPhoneNumber(phoneNumber.phone_number ?? ""),
              style: Theme.of(context).textTheme.displaySmall,
            ),
            Padding(
              padding: EdgeInsets.all(8.sp),
              child: MainButtonElevated(
                  mainButtonParameters: MainButtonParameters(
                      height: 32.sp,
                      label: "Recharge Now",
                      noShadow: true,
                      onClick: () {
                        AppNavigator.push(Routes.topupRoute,
                            arguments: {"phoneNumber": phoneNumber});
                      })),
            )
          ],
        ),
      ),
    );
  }
}
