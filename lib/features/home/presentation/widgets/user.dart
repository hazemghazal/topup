import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:topup/core/app/colors/app_color.dart';
import 'package:topup/core/util/format_numbers.dart';
import 'package:topup/core/util/sizer_custom/sizer.dart';
import 'package:topup/features/home/data/model/user_model.dart';

class ShowUser extends StatelessWidget {
  final UserModel user;
  const ShowUser({super.key, required this.user});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        SizedBox(
          width: 20.sp,
        ),
        CircleAvatar(
          backgroundColor: colorPrimary.withOpacity(0.8),
          child: const Icon(
            CupertinoIcons.person,
            color: Colors.white,
          ),
        ),
        SizedBox(
          width: 10.sp,
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Text(
                  user.name ?? "",
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.bodySmall,
                ),
                if (user.verified ?? false)
                  Column(
                    children: [
                      Icon(
                        Icons.verified,
                        color: colorPrimary,
                        size: 15.sp,
                      ),
                      SizedBox(
                        height: 5.sp,
                      )
                    ],
                  )
              ],
            ),
            Row(
              children: [
                Text(
                  "Available Credit: ",
                  style: Theme.of(context).textTheme.displaySmall,
                ),
                Text(
                  "AED " "${display(user.balance)}",
                  style: Theme.of(context).textTheme.bodySmall,
                ),
              ],
            ),
          ],
        ),
      ],
    );
  }
}
