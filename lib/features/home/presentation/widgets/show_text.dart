import 'package:flutter/material.dart';

class ShowText extends StatelessWidget {
  final String text;
  final String data;
  final Widget? icon;
  final Color? iconColor;
  final bool isGreen;
  const ShowText(
      {super.key,
      required this.text,
      required this.data,
      this.icon,
      this.isGreen = false,
      this.iconColor});

  @override
  Widget build(BuildContext context) {
    return Visibility(
      visible: data.isNotEmpty,
      child: Column(
        children: [
          const SizedBox(height: 2),
          Stack(
            children: [
              icon != null
                  ? Row(
                      children: [
                        const SizedBox(width: 10),
                        icon!,
                      ],
                    )
                  : const SizedBox(),
              Row(
                children: [
                  icon != null
                      ? const SizedBox(width: 40)
                      : const SizedBox(width: 12),
                  Expanded(
                    child: Wrap(
                      children: [
                        RichText(
                            text: TextSpan(
                          children: <TextSpan>[
                            TextSpan(
                              text: "$text  ",
                              style: Theme.of(context).textTheme.displayMedium,
                            ),
                            TextSpan(
                              text: data,
                              style: Theme.of(context).textTheme.bodyMedium,
                            ),
                          ],
                        )),
                      ],
                    ),
                  ),
                  const SizedBox(width: 8),
                ],
              ),
            ],
          ),
          const SizedBox(height: 2),
        ],
      ),
    );
  }
}
