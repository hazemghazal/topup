import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:topup/core/common/snacks.dart';
import 'package:topup/core/common/widgets/buttons/button_parameters.dart';
import 'package:topup/core/common/widgets/buttons/main_btn.dart';
import 'package:topup/core/common/widgets/label.dart';
import 'package:topup/core/injection/injection_container.dart';
import 'package:topup/core/navigator/app_pages.dart';
import 'package:topup/core/navigator/app_routes.dart';
import 'package:topup/core/util/sizer_custom/sizer.dart';
import 'package:topup/features/home/presentation/bloc/home_bloc.dart';
import 'package:topup/features/home/presentation/bloc/home_event.dart';
import 'package:topup/features/home/presentation/bloc/home_state.dart';
import 'package:topup/features/home/presentation/widgets/phone_numbers.dart';
import 'package:topup/features/home/presentation/widgets/user.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final HomeBloc homeBloc = getIt<HomeBloc>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Mobile Recharge",
          style: Theme.of(context).textTheme.headlineLarge,
        ),
        centerTitle: true,
      ),
      body: RefreshIndicator(
        onRefresh: () async {
          await Future.delayed(const Duration(seconds: 1));
        },
        child: SingleChildScrollView(
          physics: const AlwaysScrollableScrollPhysics(),
          child: BlocBuilder<HomeBloc, HomeState>(
            bloc: homeBloc,
            builder: (context, state) {
              if (state.isLoadingUser && state.isLoadingPhoneNumber) {
                return Center(
                    heightFactor: 13.sp,
                    child: const CircularProgressIndicator());
              } else {
                return Column(
                  children: [
                    if (state.isLoadingUser) ...[
                      const Padding(
                        padding: EdgeInsets.all(12),
                        child: CircularProgressIndicator(),
                      ),
                    ],
                    if (!state.isLoadingUser) ...[
                      ShowUser(
                        user: state.user,
                      ),
                    ],
                    SizedBox(
                      height: 10.sp,
                    ),
                    if (state.isLoadingPhoneNumber) ...[
                      const Padding(
                        padding: EdgeInsets.all(12),
                        child: CircularProgressIndicator(),
                      ),
                    ],
                    if (!state.isLoadingPhoneNumber) ...[
                      const LabelWidget(label: "Beneficiaries :"),
                      SizedBox(
                        height: 115.sp,
                        width: double.infinity,
                        child: ListView.builder(
                          padding: EdgeInsets.symmetric(horizontal: 16.sp),
                          itemCount: state.phoneNumbersList.length,
                          shrinkWrap: true,
                          scrollDirection: Axis.horizontal,
                          itemBuilder: ((context, index) {
                            return ShowPhoneNumber(
                              phoneNumber: state.phoneNumbersList[index],
                            );
                          }),
                        ),
                      ),
                    ],
                  ],
                );
              }
            },
          ),
        ),
      ),
      bottomSheet: Padding(
        padding: const EdgeInsets.all(10),
        child: MainButtonElevated(
          mainButtonParameters: MainButtonParameters(
              label: "Add A New Beneficiary",
              onClick: () {
                FocusManager.instance.primaryFocus?.unfocus();
                if (homeBloc.state.phoneNumbersList.length >= 5) {
                  SnackHelper.showAppDialog(
                      message:
                          "You can add a maximum of 5 top-up beneficiaries.",
                      type: SnackTypes.warning);
                  return;
                }
                AppNavigator.push(Routes.addBenRoute);
              }),
        ),
      ),
    );
  }

  @override
  void initState() {
    homeBloc.add(OnFetchUser());
    homeBloc.add(OnFetchNumbers());
    super.initState();
  }
}
