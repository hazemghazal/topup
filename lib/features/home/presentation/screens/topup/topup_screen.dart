import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:topup/core/app/colors/app_color.dart';
import 'package:topup/core/common/widgets/buttons/button_parameters.dart';
import 'package:topup/core/common/widgets/buttons/main_btn.dart';
import 'package:topup/core/injection/injection_container.dart';
import 'package:topup/core/util/format_numbers.dart';
import 'package:topup/core/util/sizer_custom/sizer.dart';
import 'package:topup/features/home/data/model/phone_number_model.dart';
import 'package:topup/features/home/domain/entities/recharge.dart';
import 'package:topup/features/home/presentation/bloc/home_bloc.dart';
import 'package:topup/features/home/presentation/bloc/home_event.dart';
import 'package:topup/features/home/presentation/bloc/home_state.dart';
import 'package:topup/features/home/presentation/widgets/show_text.dart';
import 'package:topup/features/home/presentation/widgets/user.dart';

class TopupScreen extends StatefulWidget {
  final PhoneNumberModel phoneNumber;
  const TopupScreen({super.key, required this.phoneNumber});

  @override
  State<TopupScreen> createState() => _TopupScreenState();
}

class _TopupScreenState extends State<TopupScreen> {
  List<double> amounts = [5, 10, 20, 30, 50, 75, 100];
  final HomeBloc homeBloc = getIt<HomeBloc>();

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeBloc, HomeState>(
      bloc: homeBloc,
      builder: (context, state) {
        return Scaffold(
          appBar: AppBar(
            title: Text(
              "Recharge",
              style: Theme.of(context).textTheme.headlineLarge,
            ),
            centerTitle: true,
          ),
          body: RefreshIndicator(
            onRefresh: () async {
              await Future.delayed(const Duration(seconds: 1));
            },
            child: Column(
              children: [
                ShowUser(user: state.user),
                SizedBox(
                  height: 10.sp,
                ),
                Card(
                  margin: const EdgeInsets.all(10),
                  elevation: 5,
                  child: Column(
                    children: [
                      SizedBox(
                        height: 10.sp,
                      ),
                      ShowText(
                        text: "Phone Number To Recharge: \n",
                        data: formatPhoneNumber(
                            widget.phoneNumber.phone_number ?? ""),
                      ),
                      const ShowText(
                        text: "Recharge Amount:",
                        data: " ",
                      ),
                      SizedBox(
                        height: 38.sp,
                        width: double.infinity,
                        child: ListView.builder(
                          padding: EdgeInsets.symmetric(horizontal: 16.sp),
                          itemCount: amounts.length,
                          shrinkWrap: true,
                          scrollDirection: Axis.horizontal,
                          itemBuilder: ((context, index) {
                            return Container(
                              padding: const EdgeInsets.all(10),
                              margin: const EdgeInsets.all(3),
                              decoration: BoxDecoration(
                                  color: mCU,
                                  borderRadius: BorderRadius.circular(5),
                                  border: state.selectedRechargeAmount ==
                                          amounts[index]
                                      ? Border.all(width: 1.5)
                                      : null),
                              child: GestureDetector(
                                onTap: () {
                                  homeBloc.add(
                                      OnSelectAmount(amount: amounts[index]));
                                },
                                child: Text(
                                  "AED" " " "${display(amounts[index])}",
                                  style: Theme.of(context).textTheme.bodySmall,
                                ),
                              ),
                            );
                          }),
                        ),
                      ),
                      SizedBox(
                        height: 10.sp,
                      ),
                      const ShowText(
                        text:
                            "You will be charged a 1 AED as a transaction fees",
                        data: " ",
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          bottomSheet: Padding(
            padding: const EdgeInsets.all(10),
            child: MainButtonElevated(
              mainButtonParameters: MainButtonParameters(
                  label: "Recharge",
                  isLoading: state.isLoadingRecharge,
                  onClick: () {
                    homeBloc.add(
                      OnRecharge(
                        phoneNumberModel: widget.phoneNumber,
                        param: RechargeParam(
                          id: widget.phoneNumber.phone_number!,
                          recharge_amount: state.selectedRechargeAmount,
                        ),
                      ),
                    );
                  }),
            ),
          ),
        );
      },
    );
  }
}
