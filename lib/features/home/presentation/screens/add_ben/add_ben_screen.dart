import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:topup/core/common/snacks.dart';
import 'package:topup/core/common/widgets/buttons/button_parameters.dart';
import 'package:topup/core/common/widgets/buttons/main_btn.dart';
import 'package:topup/core/injection/injection_container.dart';
import 'package:topup/features/home/domain/entities/add_ben.dart';
import 'package:topup/features/home/presentation/bloc/home_bloc.dart';
import 'package:topup/features/home/presentation/bloc/home_event.dart';
import 'package:topup/features/home/presentation/bloc/home_state.dart';
import 'package:topup/features/home/presentation/screens/add_ben/ext/screen_widgets_builder.dart';

class AddNewBeneficiary extends StatefulWidget {
  const AddNewBeneficiary({super.key});

  @override
  State<AddNewBeneficiary> createState() => AddNewBeneficiaryState();
}

class AddNewBeneficiaryState extends State<AddNewBeneficiary> {
  final formKey = GlobalKey<FormState>();
  final HomeBloc homeBloc = getIt<HomeBloc>();
  late TextEditingController txtNickName;
  late TextEditingController txtPhoneNumber;

  @override
  void initState() {
    super.initState();
    txtNickName = TextEditingController();
    txtPhoneNumber = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Add A New Beneficiary",
          style: Theme.of(context).textTheme.headlineLarge,
        ),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            form(),
          ],
        ),
      ),
      bottomSheet: Padding(
        padding: const EdgeInsets.all(10),
        child: BlocBuilder<HomeBloc, HomeState>(
          bloc: homeBloc,
          builder: (context, state) {
            return MainButtonElevated(
              mainButtonParameters: MainButtonParameters(
                  label: "Add",
                  isLoading: state.isLoadingAddBen,
                  onClick: () {
                    FocusManager.instance.primaryFocus?.unfocus();
                    if (state.phoneNumbersList.length >= 5) {
                      SnackHelper.showAppDialog(
                          message:
                              "You can add a maximum of 5 top-up beneficiaries.",
                          type: SnackTypes.warning);
                      return;
                    } else if (txtNickName.text.length > 20) {
                      SnackHelper.showAppDialog(
                          message: "Nickname maximum length is 20 letters",
                          type: SnackTypes.warning);
                      return;
                    } else if (txtPhoneNumber.text.isEmpty &&
                        txtPhoneNumber.text.length < 9) {
                      SnackHelper.showAppDialog(
                          message: "Please enter a phone number",
                          type: SnackTypes.warning);
                      return;
                    } else if (!RegExp(r'^\+?[0-9]{10,15}$')
                        .hasMatch(txtPhoneNumber.text)) {
                      SnackHelper.showAppDialog(
                          message: "Please enter a valid phone number",
                          type: SnackTypes.warning);
                      return;
                    } else {
                      homeBloc.add(OnAddBeneficiary(
                          param: AddBenParam(
                              phone_number: txtPhoneNumber.text,
                              nickname: txtNickName.text)));
                    }
                  }),
            );
          },
        ),
      ),
    );
  }
}
