import 'package:flutter/cupertino.dart';
import 'package:topup/core/app/colors/app_color.dart';
import 'package:topup/core/common/app_text_input.dart';
import 'package:topup/core/util/sizer_custom/sizer.dart';
import 'package:topup/features/home/presentation/screens/add_ben/add_ben_screen.dart';

extension WidgetsBuilder on AddNewBeneficiaryState {
  Widget form() {
    return Form(
      key: formKey,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 12),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            AppTextInput(
              appTextFieldParameters: AppTextFieldParameters(
                hintText: "Nickname",
                isRequired: true,
                controller: txtNickName,
                prefix: Icon(
                  CupertinoIcons.person,
                  color: mGB,
                ),
                textInputAction: TextInputAction.next,
                textInputType: TextInputType.text,
                suffix: const SizedBox(width: 10),
              ),
            ),
            SizedBox(
              height: 5.sp,
            ),
            AppTextInput(
              appTextFieldParameters: AppTextFieldParameters(
                hintText: "Phone Number",
                isRequired: true,
                prefix: Icon(
                  CupertinoIcons.phone,
                  color: mGB,
                ),
                controller: txtPhoneNumber,
                textInputAction: TextInputAction.next,
                textInputType: TextInputType.phone,
                suffix: const SizedBox(width: 10),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
