// ignore_for_file: non_constant_identifier_names

import 'package:json_annotation/json_annotation.dart';

part 'phone_number_model.g.dart';

@JsonSerializable(explicitToJson: true)
class PhoneNumberModel {
  String? id;
  String? nick_name;
  String? phone_number;
  double total_transactions_per_month;
  PhoneNumberModel({
    this.id,
    this.nick_name,
    this.phone_number,
    this.total_transactions_per_month = 0,
  });

  Map<String, dynamic> toJson() => _$PhoneNumberModelToJson(this);
  factory PhoneNumberModel.fromJson(Map<String, dynamic> json) =>
      _$PhoneNumberModelFromJson(json);
}
