// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'phone_number_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PhoneNumberModel _$PhoneNumberModelFromJson(Map<String, dynamic> json) =>
    PhoneNumberModel(
      id: json['id'] as String?,
      nick_name: json['nick_name'] as String?,
      phone_number: json['phone_number'] as String?,
      total_transactions_per_month:
          (json['total_transactions_per_month'] as num?)?.toDouble() ?? 0,
    );

Map<String, dynamic> _$PhoneNumberModelToJson(PhoneNumberModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'nick_name': instance.nick_name,
      'phone_number': instance.phone_number,
      'total_transactions_per_month': instance.total_transactions_per_month,
    };
