// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserModel _$UserModelFromJson(Map<String, dynamic> json) => UserModel(
      id: json['id'] as String?,
      name: json['name'] as String?,
      balance: (json['balance'] as num?)?.toDouble() ?? 0,
      total_current_month_transactions:
          (json['total_current_month_transactions'] as num?)?.toDouble() ?? 0,
      verified: json['verified'] as bool?,
    );

Map<String, dynamic> _$UserModelToJson(UserModel instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'balance': instance.balance,
      'total_current_month_transactions':
          instance.total_current_month_transactions,
      'verified': instance.verified,
    };
