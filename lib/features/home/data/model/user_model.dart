// ignore_for_file: non_constant_identifier_names

import 'package:json_annotation/json_annotation.dart';

part 'user_model.g.dart';

@JsonSerializable(explicitToJson: true)
class UserModel {
  String? id;
  String? name;
  double balance;
  double total_current_month_transactions;
  bool? verified;

  UserModel(
      {this.id,
      this.name,
      this.balance = 0,
      this.total_current_month_transactions = 0,
      this.verified});

  Map<String, dynamic> toJson() => _$UserModelToJson(this);
  factory UserModel.fromJson(Map<String, dynamic> json) =>
      _$UserModelFromJson(json);

  UserModel copyWith({
    String? id,
    String? name,
    double? balance,
    double? total_current_month_transactions,
    bool? verified,
  }) {
    return UserModel(
      id: id ?? this.id,
      name: name ?? this.name,
      balance: balance ?? this.balance,
      total_current_month_transactions: total_current_month_transactions ??
          this.total_current_month_transactions,
      verified: verified ?? this.verified,
    );
  }
}
