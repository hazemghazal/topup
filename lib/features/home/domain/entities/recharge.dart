// ignore_for_file: non_constant_identifier_names
// Dart imports:
import 'dart:convert';

class RechargeParam {
  final String id;
  final double recharge_amount;

  RechargeParam({
    required this.id,
    required this.recharge_amount,
  });

  RechargeParam copyWith({
    String? id,
    double? recharge_amount,
  }) {
    return RechargeParam(
      id: id ?? this.id,
      recharge_amount: recharge_amount ?? this.recharge_amount,
    );
  }

  Map<String, dynamic> toMap() {
    Map<String, dynamic> result = {
      'id': id,
      'recharge_amount': recharge_amount,
    };

    return result;
  }

  factory RechargeParam.fromMap(Map<String, dynamic> map) {
    return RechargeParam(
        id: map['id'] ?? '', recharge_amount: map['recharge_amount']);
  }

  String toJson() => json.encode(toMap());

  factory RechargeParam.fromJson(String source) =>
      RechargeParam.fromMap(json.decode(source));
}
