// ignore_for_file: non_constant_identifier_names
// Dart imports:
import 'dart:convert';

class AddBenParam {
  final String nickname;
  final String phone_number;

  AddBenParam({
    required this.nickname,
    required this.phone_number,
  });

  AddBenParam copyWith({
    String? nickname,
    String? phone_number,
  }) {
    return AddBenParam(
      nickname: nickname ?? this.nickname,
      phone_number: phone_number ?? this.phone_number,
    );
  }

  Map<String, dynamic> toMap() {
    Map<String, dynamic> result = {
      'nickname': nickname,
      'phone_number': phone_number,
    };

    return result;
  }

  factory AddBenParam.fromMap(Map<String, dynamic> map) {
    return AddBenParam(
        nickname: map['nickname'] ?? '', phone_number: map['phone_number']);
  }

  String toJson() => json.encode(toMap());

  factory AddBenParam.fromJson(String source) =>
      AddBenParam.fromMap(json.decode(source));
}
