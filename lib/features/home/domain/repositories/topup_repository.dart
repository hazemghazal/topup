// Package imports:
import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:topup/core/app/config/http_manager.dart';
import 'package:topup/core/injection/injection_container.dart';
import 'package:topup/features/home/data/model/phone_number_model.dart';
import 'package:topup/features/home/data/model/user_model.dart';
import 'package:topup/features/home/domain/entities/add_ben.dart';
import 'package:topup/features/home/domain/entities/recharge.dart';
import 'package:uuid/uuid.dart';

// Project imports:

abstract class TopupRepository {
  Future<Either<String, UserModel>> getUser();
  Future<Either<String, List<PhoneNumberModel>>> getPhoneNumbers();
  Future<Either<String, PhoneNumberModel>> addBen({required AddBenParam param});
  Future<Either<String, String>> recharge({required RechargeParam param});
}

@LazySingleton(as: TopupRepository)
class TopupRepositoryImpl implements TopupRepository {
  final HTTPManager httpManager = getIt<HTTPManager>();
  @override
  Future<Either<String, List<PhoneNumberModel>>> getPhoneNumbers() {
    /// Implement Http Request
    // final result =  httpManager.get(url: Endpoints.phoneNumbers);
    // Dummy Data
    final dynamic result = [
      {
        "id": "1b2e35f4-5678-91ab-cdef-12345d6789ab",
        "nick_name": "John Doe",
        "phone_number": "0501234567"
      },
      {
        "id": "2c3d45e6-7891-01ab-cdef-23456e7890bc",
        "nick_name": "Jane Smith",
        "phone_number": "0501234568"
      },
      {
        "id": "3d4e56f7-8910-12ab-cdef-34567f8901cd",
        "nick_name": "Mike Johnson",
        "phone_number": "0501234569"
      },
      {
        "id": "4e5f67g8-9101-23ab-cdef-45678g9012de",
        "nick_name": "Emily Davis",
        "phone_number": "0501234570"
      }
    ];

    /// Handel if Left
    //  return Future(() => Left(userFake));

    // else
    Iterable l = result;
    List<PhoneNumberModel> data = List<PhoneNumberModel>.from(
        l.map((model) => PhoneNumberModel.fromJson(model)));
    return Future(() => Right(data));
  }

  @override
  Future<Either<String, UserModel>> getUser() {
    /// Implement Http Request
    // final result =  httpManager.get(url: Endpoints.user);
    // Dummy Data
    final dynamic result = {
      "id": const Uuid().v4(),
      "name": 'Brody',
      "balance": 100,
      "verified": false,
      "total_current_month_transactions": 10,
    };

    /// Handel if Left
    //  return Future(() => Left(userFake));

    // else
    return Future(() => Right(UserModel.fromJson(result)));
  }

  @override
  Future<Either<String, PhoneNumberModel>> addBen(
      {required AddBenParam param}) {
    /// Implement Http Request
    // final result =  httpManager.post(url: Endpoints.addBeneficiary, data: param.toJson());
    // Dummy Data
    final dynamic result = {
      'message': 'Beneficiary is added successfully',
      'beneficiary': {
        "id": Uuid().v4(),
        "nick_name": param.nickname,
        "phone_number": param.phone_number
      },
    };

    /// Handel if Left
    //  return Future(() => Left(result['error']['message']));

    // else
    return Future(
        () => Right(PhoneNumberModel.fromJson(result['beneficiary'])));
  }

  @override
  Future<Either<String, String>> recharge({required RechargeParam param}) {
    /// Implement Http Request
    // final result =  httpManager.post(url: Endpoints.user, data: param.toJson());
    // Dummy Data
    final dynamic result = {
      'message': 'Recharged successfully',
    };

    /// Handel if Left
    //  return Future(() => Left(result['error']['message']));

    // else
    return Future(() => Right(result['message']));
  }
}
