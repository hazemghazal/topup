// Dart imports:
// Flutter imports:
import 'package:flutter/material.dart';
import 'package:topup/core/app/themes/themes.dart';
import 'package:topup/core/navigator/app_pages.dart';
import 'package:topup/core/util/sizer_custom/sizer.dart';
import 'package:topup/features/home/presentation/screens/home/home_screen.dart';

class App extends StatefulWidget {
  const App({super.key});

  @override
  State<StatefulWidget> createState() {
    return _AppState();
  }
}

class _AppState extends State<App> {
  @override
  Widget build(BuildContext context) {
    return Sizer(
      builder: ((context, orientation, deviceType) {
        return MaterialApp(
          navigatorKey: AppNavigator.navigatorKey,
          debugShowCheckedModeBanner: false,
          theme: AppTheme.light().data,
          themeMode: ThemeMode.dark,
          navigatorObservers: [NavigatorObserver()],
          onGenerateRoute: (settings) {
            return AppNavigator().getRoute(settings);
          },
          home: const HomeScreen(),
        );
      }),
    );
  }
}
